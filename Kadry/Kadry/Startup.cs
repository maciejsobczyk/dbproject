﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kadry.Startup))]
namespace Kadry
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
