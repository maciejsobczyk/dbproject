﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kadry;

namespace Kadry.Controllers
{
    public class addressesController : Controller
    {
        private Model1Container db = new Model1Container();

        // GET: addresses
        public ActionResult Index()
        {
            var addressSet = db.addressSet.Include(a => a.employee);
            return View(addressSet.ToList());
        }

        // GET: addresses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            address address = db.addressSet.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // GET: addresses/Create
        public ActionResult Create()
        {
            ViewBag.employee_idemployee = new SelectList(db.employeeSet, "idemployee", "name");
            return View();
        }

        // POST: addresses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idaddress,town,postalcode,street,buldingnumber,phonenumber,employee_idemployee")] address address)
        {
            if (ModelState.IsValid)
            {
                db.addressSet.Add(address);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.employee_idemployee = new SelectList(db.employeeSet, "idemployee", "name", address.employee_idemployee);
            return View(address);
        }

        // GET: addresses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            address address = db.addressSet.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            ViewBag.employee_idemployee = new SelectList(db.employeeSet, "idemployee", "name", address.employee_idemployee);
            return View(address);
        }

        // POST: addresses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idaddress,town,postalcode,street,buldingnumber,phonenumber,employee_idemployee")] address address)
        {
            if (ModelState.IsValid)
            {
                db.Entry(address).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.employee_idemployee = new SelectList(db.employeeSet, "idemployee", "name", address.employee_idemployee);
            return View(address);
        }

        // GET: addresses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            address address = db.addressSet.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // POST: addresses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            address address = db.addressSet.Find(id);
            db.addressSet.Remove(address);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
