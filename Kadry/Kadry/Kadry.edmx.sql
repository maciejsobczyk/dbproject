
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/12/2018 22:58:15
-- Generated from EDMX file: C:\Users\Maciej\Documents\dbproject\Kadry\Kadry\Kadry.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [kadry];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_technologyemployee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[employeeSet] DROP CONSTRAINT [FK_technologyemployee];
GO
IF OBJECT_ID(N'[dbo].[FK_postionemployee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[employeeSet] DROP CONSTRAINT [FK_postionemployee];
GO
IF OBJECT_ID(N'[dbo].[FK_employeeaddress]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[addressSet] DROP CONSTRAINT [FK_employeeaddress];
GO
IF OBJECT_ID(N'[dbo].[FK_employeeemployeeschedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[employeescheduleSet] DROP CONSTRAINT [FK_employeeemployeeschedule];
GO
IF OBJECT_ID(N'[dbo].[FK_scheduleemployeeschedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[employeescheduleSet] DROP CONSTRAINT [FK_scheduleemployeeschedule];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[postionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[postionSet];
GO
IF OBJECT_ID(N'[dbo].[technologySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[technologySet];
GO
IF OBJECT_ID(N'[dbo].[addressSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[addressSet];
GO
IF OBJECT_ID(N'[dbo].[employeescheduleSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[employeescheduleSet];
GO
IF OBJECT_ID(N'[dbo].[employeeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[employeeSet];
GO
IF OBJECT_ID(N'[dbo].[scheduleSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[scheduleSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'postionSet'
CREATE TABLE [dbo].[postionSet] (
    [idposition] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(20)  NOT NULL
);
GO

-- Creating table 'technologySet'
CREATE TABLE [dbo].[technologySet] (
    [idtechnology] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(20)  NOT NULL
);
GO

-- Creating table 'addressSet'
CREATE TABLE [dbo].[addressSet] (
    [idaddress] int IDENTITY(1,1) NOT NULL,
    [town] nvarchar(15)  NOT NULL,
    [postalcode] nvarchar(6)  NOT NULL,
    [street] nvarchar(15)  NOT NULL,
    [buldingnumber] nvarchar(6)  NOT NULL,
    [phonenumber] nvarchar(6)  NOT NULL,
    [employee_idemployee] int  NOT NULL
);
GO

-- Creating table 'employeescheduleSet'
CREATE TABLE [dbo].[employeescheduleSet] (
    [idemployeeschedule] int IDENTITY(1,1) NOT NULL,
    [daytype] smallint  NOT NULL,
    [employee_idemployee] int  NOT NULL,
    [schedule_idschedule] int  NOT NULL
);
GO

-- Creating table 'employeeSet'
CREATE TABLE [dbo].[employeeSet] (
    [idemployee] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(10)  NOT NULL,
    [surname] nvarchar(15)  NOT NULL,
    [hourlyrate] decimal(6,2)  NOT NULL,
    [holidays] smallint  NOT NULL,
    [isactive] bit  NOT NULL,
    [technology_idtechnology] int  NOT NULL,
    [postion_idposition] int  NOT NULL
);
GO

-- Creating table 'scheduleSet'
CREATE TABLE [dbo].[scheduleSet] (
    [idschedule] int IDENTITY(1,1) NOT NULL,
    [date] datetime  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [idposition] in table 'postionSet'
ALTER TABLE [dbo].[postionSet]
ADD CONSTRAINT [PK_postionSet]
    PRIMARY KEY CLUSTERED ([idposition] ASC);
GO

-- Creating primary key on [idtechnology] in table 'technologySet'
ALTER TABLE [dbo].[technologySet]
ADD CONSTRAINT [PK_technologySet]
    PRIMARY KEY CLUSTERED ([idtechnology] ASC);
GO

-- Creating primary key on [idaddress] in table 'addressSet'
ALTER TABLE [dbo].[addressSet]
ADD CONSTRAINT [PK_addressSet]
    PRIMARY KEY CLUSTERED ([idaddress] ASC);
GO

-- Creating primary key on [idemployeeschedule] in table 'employeescheduleSet'
ALTER TABLE [dbo].[employeescheduleSet]
ADD CONSTRAINT [PK_employeescheduleSet]
    PRIMARY KEY CLUSTERED ([idemployeeschedule] ASC);
GO

-- Creating primary key on [idemployee] in table 'employeeSet'
ALTER TABLE [dbo].[employeeSet]
ADD CONSTRAINT [PK_employeeSet]
    PRIMARY KEY CLUSTERED ([idemployee] ASC);
GO

-- Creating primary key on [idschedule] in table 'scheduleSet'
ALTER TABLE [dbo].[scheduleSet]
ADD CONSTRAINT [PK_scheduleSet]
    PRIMARY KEY CLUSTERED ([idschedule] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [technology_idtechnology] in table 'employeeSet'
ALTER TABLE [dbo].[employeeSet]
ADD CONSTRAINT [FK_technologyemployee]
    FOREIGN KEY ([technology_idtechnology])
    REFERENCES [dbo].[technologySet]
        ([idtechnology])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_technologyemployee'
CREATE INDEX [IX_FK_technologyemployee]
ON [dbo].[employeeSet]
    ([technology_idtechnology]);
GO

-- Creating foreign key on [postion_idposition] in table 'employeeSet'
ALTER TABLE [dbo].[employeeSet]
ADD CONSTRAINT [FK_postionemployee]
    FOREIGN KEY ([postion_idposition])
    REFERENCES [dbo].[postionSet]
        ([idposition])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_postionemployee'
CREATE INDEX [IX_FK_postionemployee]
ON [dbo].[employeeSet]
    ([postion_idposition]);
GO

-- Creating foreign key on [employee_idemployee] in table 'addressSet'
ALTER TABLE [dbo].[addressSet]
ADD CONSTRAINT [FK_employeeaddress]
    FOREIGN KEY ([employee_idemployee])
    REFERENCES [dbo].[employeeSet]
        ([idemployee])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_employeeaddress'
CREATE INDEX [IX_FK_employeeaddress]
ON [dbo].[addressSet]
    ([employee_idemployee]);
GO

-- Creating foreign key on [employee_idemployee] in table 'employeescheduleSet'
ALTER TABLE [dbo].[employeescheduleSet]
ADD CONSTRAINT [FK_employeeemployeeschedule]
    FOREIGN KEY ([employee_idemployee])
    REFERENCES [dbo].[employeeSet]
        ([idemployee])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_employeeemployeeschedule'
CREATE INDEX [IX_FK_employeeemployeeschedule]
ON [dbo].[employeescheduleSet]
    ([employee_idemployee]);
GO

-- Creating foreign key on [schedule_idschedule] in table 'employeescheduleSet'
ALTER TABLE [dbo].[employeescheduleSet]
ADD CONSTRAINT [FK_scheduleemployeeschedule]
    FOREIGN KEY ([schedule_idschedule])
    REFERENCES [dbo].[scheduleSet]
        ([idschedule])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_scheduleemployeeschedule'
CREATE INDEX [IX_FK_scheduleemployeeschedule]
ON [dbo].[employeescheduleSet]
    ([schedule_idschedule]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------