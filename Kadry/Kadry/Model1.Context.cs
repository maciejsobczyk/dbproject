﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kadry
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Model1Container : DbContext
    {
        public Model1Container()
            : base("name=Model1Container")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<postion> postionSet { get; set; }
        public virtual DbSet<technology> technologySet { get; set; }
        public virtual DbSet<address> addressSet { get; set; }
        public virtual DbSet<employeeschedule> employeescheduleSet { get; set; }
        public virtual DbSet<employee> employeeSet { get; set; }
        public virtual DbSet<schedule> scheduleSet { get; set; }
    }
}
