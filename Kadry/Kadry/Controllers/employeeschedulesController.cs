﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kadry;

namespace Kadry.Controllers
{
    public class employeeschedulesController : Controller
    {
        private Model1Container db = new Model1Container();

        // GET: employeeschedules
        public ActionResult Index()
        {
            var employeescheduleSet = db.employeescheduleSet.Include(e => e.employee).Include(e => e.schedule);
            return View(employeescheduleSet.ToList());
        }

        // GET: employeeschedules/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employeeschedule employeeschedule = db.employeescheduleSet.Find(id);
            if (employeeschedule == null)
            {
                return HttpNotFound();
            }
            return View(employeeschedule);
        }

        // GET: employeeschedules/Create
        public ActionResult Create()
        {
            ViewBag.employee_idemployee = new SelectList(db.employeeSet, "idemployee", "name");
            ViewBag.schedule_idschedule = new SelectList(db.scheduleSet, "idschedule", "idschedule");
            return View();
        }

        // POST: employeeschedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idemployeeschedule,daytype,employee_idemployee,schedule_idschedule")] employeeschedule employeeschedule)
        {
            if (ModelState.IsValid)
            {
                db.employeescheduleSet.Add(employeeschedule);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.employee_idemployee = new SelectList(db.employeeSet, "idemployee", "name", employeeschedule.employee_idemployee);
            ViewBag.schedule_idschedule = new SelectList(db.scheduleSet, "idschedule", "idschedule", employeeschedule.schedule_idschedule);
            return View(employeeschedule);
        }

        // GET: employeeschedules/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employeeschedule employeeschedule = db.employeescheduleSet.Find(id);
            if (employeeschedule == null)
            {
                return HttpNotFound();
            }
            ViewBag.employee_idemployee = new SelectList(db.employeeSet, "idemployee", "name", employeeschedule.employee_idemployee);
            ViewBag.schedule_idschedule = new SelectList(db.scheduleSet, "idschedule", "idschedule", employeeschedule.schedule_idschedule);
            return View(employeeschedule);
        }

        // POST: employeeschedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idemployeeschedule,daytype,employee_idemployee,schedule_idschedule")] employeeschedule employeeschedule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employeeschedule).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.employee_idemployee = new SelectList(db.employeeSet, "idemployee", "name", employeeschedule.employee_idemployee);
            ViewBag.schedule_idschedule = new SelectList(db.scheduleSet, "idschedule", "idschedule", employeeschedule.schedule_idschedule);
            return View(employeeschedule);
        }

        // GET: employeeschedules/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employeeschedule employeeschedule = db.employeescheduleSet.Find(id);
            if (employeeschedule == null)
            {
                return HttpNotFound();
            }
            return View(employeeschedule);
        }

        // POST: employeeschedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            employeeschedule employeeschedule = db.employeescheduleSet.Find(id);
            db.employeescheduleSet.Remove(employeeschedule);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
