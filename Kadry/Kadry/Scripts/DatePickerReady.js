﻿$(function () {
    $("#datepicker").datetimepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
        dateFormat: 'mm/dd/yy',
        controlType: 'select',
        timeFormat: 'hh:mm TT',

    });
});