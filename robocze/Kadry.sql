------------------------------------------- TWORZENIE TABEL

-- TABELA 'positionSet'
CREATE TABLE positionSet
(
    idposition int IDENTITY(1,1) NOT NULL,
    name nvarchar(20)  NOT NULL
);
GO

-- TABELA 'technologySet'
CREATE TABLE technologySet
(
    idtechnology int IDENTITY(1,1) NOT NULL,
    name nvarchar(20)  NOT NULL
);
GO

-- TABELA 'addressSet'
CREATE TABLE addressSet (
    idaddress int IDENTITY(1,1) NOT NULL,
    town nvarchar(105)  NOT NULL,
    postalcode nvarchar(6)  NOT NULL,
    street nvarchar(105)  NOT NULL,
    buldingnumber nvarchar(6)  NOT NULL,
    phonenumber nvarchar(12)  NOT NULL,
    employee_idemployee int  NOT NULL
);
GO

-- TABELA 'employeescheduleSet'
CREATE TABLE employeescheduleSet (
    idemployeeschedule int IDENTITY(1,1) NOT NULL,
    daytype smallint  NOT NULL,
    employee_idemployee int  NOT NULL,
    schedule_idschedule int  NOT NULL
);
GO

-- TABELA 'employeeSet'
CREATE TABLE employeeSet (
    idemployee int IDENTITY(1,1) NOT NULL,
    name nvarchar(10)  NOT NULL,
    surname nvarchar(15)  NOT NULL,
    hourlyrate decimal(6,2)  NOT NULL,
    holidays smallint  NOT NULL,
    isactive bit  NOT NULL,
    technology_idtechnology int  NOT NULL,
    position_idposition int  NOT NULL
);
GO

-- TABELA 'scheduleSet'
CREATE TABLE scheduleSet (
    idschedule int IDENTITY(1,1) NOT NULL,
    date datetime  NOT NULL
);
GO

------------------------------------------- TWORZENIE KLUCZY GŁÓWNYCH

ALTER TABLE positionSet
ADD CONSTRAINT PK_positionSet
    PRIMARY KEY (idposition);
GO

-- TABELA 'technologySet'
ALTER TABLE technologySet
ADD CONSTRAINT PK_technologySet
    PRIMARY KEY (idtechnology);
GO

-- TABELA 'addressSet'
ALTER TABLE addressSet
ADD CONSTRAINT PK_addressSet
    PRIMARY KEY (idaddress);
GO

-- TABELA 'employeescheduleSet'
ALTER TABLE employeescheduleSet
ADD CONSTRAINT PK_employeescheduleSet
    PRIMARY KEY (idemployeeschedule);
GO

-- TABELA 'employeeSet'
ALTER TABLE employeeSet
ADD CONSTRAINT PK_employeeSet
    PRIMARY KEY (idemployee);
GO

-- TABELA 'scheduleSet'
ALTER TABLE scheduleSet
ADD CONSTRAINT PK_scheduleSet
    PRIMARY KEY (idschedule);
GO


------------------------------------------- TWORZENIE KLUCZY OBCYCH

-- TABELA 'employeeSet'
ALTER TABLE employeeSet
ADD CONSTRAINT FK_technologyemployee
    FOREIGN KEY (technology_idtechnology)
    REFERENCES technologySet (idtechnology)
GO

-- TABELA 'employeeSet'
ALTER TABLE employeeSet
ADD CONSTRAINT FK_positionemployee
    FOREIGN KEY (position_idposition)
    REFERENCES positionSet (idposition)
GO

-- TABELA 'addressSet'
ALTER TABLE addressSet
ADD CONSTRAINT FK_employeeaddress
    FOREIGN KEY (employee_idemployee)
    REFERENCES employeeSet (idemployee)
GO

-- TABELA 'employeescheduleSet'
ALTER TABLE employeescheduleSet
ADD CONSTRAINT FK_employeeemployeeschedule
    FOREIGN KEY (employee_idemployee)
    REFERENCES employeeSet (idemployee)
GO

-- TABELA 'employeescheduleSet'
ALTER TABLE employeescheduleSet
ADD CONSTRAINT FK_scheduleemployeeschedule
    FOREIGN KEY (schedule_idschedule)
    REFERENCES scheduleSet (idschedule)
GO