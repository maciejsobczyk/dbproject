﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kadry;

namespace Kadry.Controllers
{
    public class postionsController : Controller
    {
        private Model1Container db = new Model1Container();

        // GET: postions
        public ActionResult Index()
        {
            return View(db.postionSet.ToList());
        }

        // GET: postions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            postion postion = db.postionSet.Find(id);
            if (postion == null)
            {
                return HttpNotFound();
            }
            return View(postion);
        }

        // GET: postions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: postions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idposition,name")] postion postion)
        {
            if (ModelState.IsValid)
            {
                db.postionSet.Add(postion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(postion);
        }

        // GET: postions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            postion postion = db.postionSet.Find(id);
            if (postion == null)
            {
                return HttpNotFound();
            }
            return View(postion);
        }

        // POST: postions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idposition,name")] postion postion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(postion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(postion);
        }

        // GET: postions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            postion postion = db.postionSet.Find(id);
            if (postion == null)
            {
                return HttpNotFound();
            }
            return View(postion);
        }

        // POST: postions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            postion postion = db.postionSet.Find(id);
            db.postionSet.Remove(postion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
