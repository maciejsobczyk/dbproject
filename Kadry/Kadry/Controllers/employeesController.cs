﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kadry;

namespace Kadry.Controllers
{
    public class employeesController : Controller
    {
        private Model1Container db = new Model1Container();

        // GET: employees
        public ActionResult Index()
        {
            var employeeSet = db.employeeSet.Include(e => e.technology).Include(e => e.postion);
            return View(employeeSet.ToList());
        }

        // GET: employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employeeSet.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: employees/Create
        public ActionResult Create()
        {
            ViewBag.technology_idtechnology = new SelectList(db.technologySet, "idtechnology", "name");
            ViewBag.postion_idposition = new SelectList(db.postionSet, "idposition", "name");
            return View();
        }

        // POST: employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idemployee,name,surname,hourlyrate,holidays,isactive,technology_idtechnology,postion_idposition")] employee employee)
        {
            if (ModelState.IsValid)
            {
                db.employeeSet.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.technology_idtechnology = new SelectList(db.technologySet, "idtechnology", "name", employee.technology_idtechnology);
            ViewBag.postion_idposition = new SelectList(db.postionSet, "idposition", "name", employee.postion_idposition);
            return View(employee);
        }

        // GET: employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employeeSet.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.technology_idtechnology = new SelectList(db.technologySet, "idtechnology", "name", employee.technology_idtechnology);
            ViewBag.postion_idposition = new SelectList(db.postionSet, "idposition", "name", employee.postion_idposition);
            return View(employee);
        }

        // POST: employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idemployee,name,surname,hourlyrate,holidays,isactive,technology_idtechnology,postion_idposition")] employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.technology_idtechnology = new SelectList(db.technologySet, "idtechnology", "name", employee.technology_idtechnology);
            ViewBag.postion_idposition = new SelectList(db.postionSet, "idposition", "name", employee.postion_idposition);
            return View(employee);
        }

        // GET: employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employeeSet.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            employee employee = db.employeeSet.Find(id);
            db.employeeSet.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
